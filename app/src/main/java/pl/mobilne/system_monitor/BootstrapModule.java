package pl.mobilne.system_monitor;

import android.accounts.AccountManager;
import android.content.Context;

import pl.mobilne.system_monitor.authenticator.BootstrapAuthenticatorActivity;
import pl.mobilne.system_monitor.authenticator.LogoutService;
import pl.mobilne.system_monitor.core.TimerService;
import pl.mobilne.system_monitor.ui.BootstrapTimerActivity;
import pl.mobilne.system_monitor.ui.MainActivity;
import pl.mobilne.system_monitor.ui.CheckInsListFragment;
import pl.mobilne.system_monitor.ui.NavigationDrawerFragment;
import pl.mobilne.system_monitor.ui.NewsActivity;
import pl.mobilne.system_monitor.ui.NewsListFragment;
import pl.mobilne.system_monitor.ui.UserActivity;
import pl.mobilne.system_monitor.ui.UserListFragment;
import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Dagger module for setting up provides statements.
 * Register all of your entry points below.
 */
@Module(
        complete = false,

        injects = {
                BootstrapApplication.class,
                BootstrapAuthenticatorActivity.class,
                MainActivity.class,
                BootstrapTimerActivity.class,
                CheckInsListFragment.class,
                NavigationDrawerFragment.class,
                NewsActivity.class,
                NewsListFragment.class,
                UserActivity.class,
                UserListFragment.class,
                TimerService.class
        }
)
public class BootstrapModule {

    @Singleton
    @Provides
    Bus provideOttoBus() {
        return new Bus();
    }

    @Provides
    @Singleton
    LogoutService provideLogoutService(final Context context, final AccountManager accountManager) {
        return new LogoutService(context, accountManager);
    }

}
